#!/usr/bin/python3

import xlsxwriter
import datetime
import json
import argparse


class XlsxRow:
    def __init__(self, config):
        self.date = None
        self.weekday = None
        self.start_time = None
        self.end_time = None
        self.lunch_time = None
        self.config = config    # Мы можем обойтись без этой строки?

    def calculate_dur(self):
        if self.start_time is not None:
            s = datetime.time.fromisoformat(self.start_time)
            e = datetime.time.fromisoformat(self.end_time)
            date_ = datetime.date(1, 1, 1)
            datetime1 = datetime.datetime.combine(date_, s)
            datetime2 = datetime.datetime.combine(date_, e)
            dur = (datetime2 - datetime1) - self.lunch_duration()   # class 'datetime.timedelta'
            return dur
        return None

    def lunch_duration(self):
        # here we turn string from config file
        # into timedelta object
        a, b = self.lunch_time.split(':')
        a, b = int(a), int(b)
        if a > 0:
            a *= 3600  # hours
        if b > 0:
            b *= 60    # minutes
        return datetime.timedelta(seconds=(a+b))


def lalala(row_list):
    total = datetime.timedelta()
    for r in row_list:
        d = r.calculate_dur()
        if d is not None:
            total += d
    print(total)
    days = total.days
    sec = total.seconds
    t = (days * 24) + (sec // 3600)
    minutes = (sec//60) % 60
    return '{}:{}'.format(t, minutes)


def date_from_str(string):
    month_name = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6,
                  "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12}
    (day), month, year = string.split(sep='-')
    return datetime.date(int(year), month_name[month], int(day))


class Config:
    def __init__(self, config, holidays):
        self.config = config
        self.weekends = []
        self.holidays = []
        day_name = {"Monday": 0, "Tuesday": 1, "Wednesday": 2,
                    "Thursday": 3, "Friday": 4, "Saturday": 5, "Sunday": 6}
        for w in self.config.get('weekends'):
            self.weekends.append(day_name[w])
        for h in holidays:
            self.holidays.append(date_from_str(h))

    def get(self, name):
        return self.config.get(name)

    def is_workday(self, my_date):
        if my_date.weekday() in self.weekends:
            return False
        if my_date in self.holidays:
            return False
        return True


def list_of_dates(year, month, confg):
    my_date = datetime.date(year, month, 1)
    delta = datetime.timedelta(days=1)
    dates = []

    while my_date.month == month:
        row = XlsxRow(confg)
        row.date = my_date.strftime('%d-%b-%Y')
        row.weekday = my_date.strftime('%A')
        row.lunch_time = confg.get('lunch_time')
        if confg.is_workday(my_date) is True:
            row.start_time = confg.get('start_time')
            row.end_time = confg.get('end_time')
        dates.append(row)
        my_date += delta

    return dates


def write_to_excel(row_list):
    with xlsxwriter.Workbook('_report.xlsx') as workbook:
        worksheet = workbook.add_worksheet()
        worksheet.set_column('A:A', 14)
        worksheet.set_column('B:B', 11)
        worksheet.set_column('F:F', 10)
        row = 1
        col = 0
        # write headlines
        worksheet.write(row - 1, col, 'Date')
        worksheet.write(row - 1, col + 2, 'Start')
        worksheet.write(row - 1, col + 3, 'End')
        worksheet.write(row - 1, col + 4, 'Duration')
        worksheet.write(row - 1, col + 5, 'Lunch time')
        worksheet.write(row,     col + 5, row_list[0].lunch_time)
        for r in row_list:
            # write data
            worksheet.write(row, col,     r.date)
            worksheet.write(row, col + 1, r.weekday)
            worksheet.write(row, col + 2, r.start_time)
            worksheet.write(row, col + 3, r.end_time)
            d = r.calculate_dur()
            if d is not None:
                worksheet.write(row, col + 4, str(d)[:-3])
            row += 1
        worksheet.write(row + 2, col + 2, 'Total')
        worksheet.write(row + 3, col + 2, lalala(row_list))


# read some data from json file
def json_handler(file_path):
    with open(file_path) as file:
        data = json.load(file)
        return data


if __name__ == '__main__':
    today = datetime.date.today()
    parser = argparse.ArgumentParser(description='some text')
    parser.add_argument('--user_config', nargs='?',
                        help='Specifies user settings file',
                        default='user.json')
    parser.add_argument('--year', type=int,
                        help='Specifies year for the report',
                        default=today.year)
    parser.add_argument('--month', type=int,
                        help='Specifies month for the report',
                        default=today.month)
    args = parser.parse_args()

    x = list_of_dates(args.year, args.month, Config(json_handler(args.user_config), json_handler('holidays.json')))
    write_to_excel(row_list=x)
    print('It works')
