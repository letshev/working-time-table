"""
Generating working time table in markdown .md file format
"""

from __future__ import annotations
from calendar import Calendar
import datetime
import re


class Question:
    """Just text, questions in str format"""
    NAME          = "\n1) Your first and last name: "
    YEAR_MONTH    = "\n2) Year and month, separated by dash (e.g.: 2022-01): "
    WORKDAY_START = "\n3) Typical start of the working day (HH:MM in 24-h format): "
    WORKDAY_END   = "\n4) Typical end of the working day (HH:MM in 24-h format): "
    LUNCH         = "\n4) Working day lunch duration in minutes: "


class Answers:
    """Class for storing and validating data, obtained from user"""

    def check_empty(self, argument: str):
        """Check if `argument` is empty value"""
        if not argument or argument.isspace():
            raise ValueError("value should not be empty!")

    def check_correct_time(self, time):
        """Check if time `time` is in HH:MM format"""
        hours, minutes = time.split(":")
        if len(hours) != 2 or len(minutes) != 2:
            raise ValueError("Incorrect time format")
        if int(hours) > 23 or int(hours) < 0:
            raise ValueError(f"Incorrect hours: {hours}")
        if int(minutes) > 59 or int(minutes) < 0:
            raise ValueError(f"Incorrect minutes: {minutes}")

    @property  # 1
    def name(self) -> str:
        """Name of the user for the first row in output file."""
        return self._name

    @name.setter
    def name(self, name):
        self.check_empty(name)
        # regex for case-insensitive search
        pattern = r"^[a-zßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿąćčėęįłńœšūųźżž∂]+(?:(?:[',\. -]"\
                  r"[a-zßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿąćčėęįłńœšūųźżž∂ ])?"\
                  r"[a-zßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿąćčėęįłńœšūųźżž∂\.]*)*$"
        match = re.search(pattern, name, re.IGNORECASE)
        if not match:
            raise ValueError("Wrong characters in name!")
        self._name = name

    @property  # 2
    def year_month(self) -> str:
        """Year and month in format YYYY-MM"""
        return self._year_month

    @year_month.setter
    def year_month(self, ym: str):
        self.check_empty(ym)
        # check correctness of year and month
        year, month = ym.split("-")
        year = int(year)
        month = int(month)
        if year < 2000 or year > 2050:
            raise ValueError(f"Wrong year value: {year}")
        if month > 12 or month < 1:
            raise ValueError(f"Wrong month value: {month}")
        self._year_month = ym

    @property  # 3
    def start_time(self) -> str:
        """Start time of the working day, HH:MM"""
        return self._start_time

    @start_time.setter
    def start_time(self, start: str):
        self.check_empty(start)
        self.check_correct_time(start)
        self._start_time = start

    @property  # 4
    def end_time(self) -> str:
        """End time of the working day, HH:MM"""
        return self._end_time

    @end_time.setter
    def end_time(self, end: str):
        self.check_empty(end)
        self.check_correct_time(end)
        self._end_time = end

    @property  # 5
    def lunch(self) -> str:
        """Lunch time in minutes, for one typical working day."""
        return self._lunch

    @lunch.setter
    def lunch(self, lunch_time: str):
        self.check_empty(lunch_time)
        if not lunch_time.isdigit():
            raise ValueError("Lunch time should contain only digit characters!")
        self._lunch = lunch_time


def work_duration_day(starttime: str, endtime: str, lunch: str) -> datetime.timedelta:
    """
    Return one working day duration, namely the differense between endtime and (starttime + lunch)
    :note: non-standard working days, when the beginning of the working day and the end of the
    working day are on different calendar days, are not taken into account in the calculations.
    """
    dt_start = datetime.datetime.strptime(starttime, '%H:%M')
    dt_end = datetime.datetime.strptime(endtime, '%H:%M')
    dt_lunch = datetime.timedelta(minutes=int(lunch))
    if (dt_start + dt_lunch) >= dt_end:
        raise ValueError("start time + lunch time cannot be >= end time")
    return dt_end - (dt_start + dt_lunch)


def work_duration_total(iterable: list[DayRow]) -> float:
    """
    Return the total duration of working time for some period of time (e.g. one month).
    """
    total = datetime.timedelta(seconds=0)
    for day in iterable:
        if not day.holiday:
            total = total + day.duration_timedelta
    return total.total_seconds()


def generate_day_rows(year: str, month: str, start: str, end: str, lunch: str) -> list[DayRow]:
    """
    Generate list of DayRow instances, where each instace stores information about each day of
    the month `month`.
    :param year: year, used to generate list of days (DayRow instances)
    :param month: month, used to generate list of days (DayRow instances) in year `year`
    :param start: start time of each working day, HH:MM
    :param end: end time of each working day, HH:MM
    :param lunch: lunch time, minutes per day
    """
    year = int(year)
    month = int(month)
    month: list(datetime.date) = [elem for elem in Calendar().itermonthdates(year, month) if elem.month == month]
    return [DayRow(day, start, end, lunch) for day in month]


class DayRow:
    """
    Storing info about one day. str(DayRow instance) represents row in Markdown table format
    """
    weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    def __init__(self, date: datetime.date, start: str, end: str, lunch: str, holiday: bool = False) -> None:
        """
        :arg date: the date for which to generate all necessary information
        :arg start: srart of the working day, HH:MM
        :arg end: end of the working day, HH:MM
        :arg lunch: lunch time, in minutes
        :arg holiday: True if this is non-working day, and False if this is working day
        """
        self._date = date
        self._start = start
        self._end = end
        self._lunch = lunch
        # holidays
        default_holidays = (DayRow.weekdays[5], DayRow.weekdays[6])  # "Saturday" and "Sunday"
        self._holiday = (self.weekday in default_holidays) if not holiday else holiday

    @property
    def holiday(self) -> bool:
        return self._holiday

    @property
    def date(self) -> str:
        """str in format DD-Mmm-YYYY, e.g. 01-Jan-2022"""
        return self._date.strftime("%d-%b-%Y")

    @property
    def weekday(self) -> str:
        """Text name of the day of the week, e.g. Saturday"""
        return DayRow.weekdays[self._date.weekday()]

    @property
    def start(self) -> str:
        """If working day: string in format HH:MM, if holiday: empty string"""
        return "" if self._holiday else self._start

    @property
    def end(self) -> str:
        """If working day: string in format HH:MM, if holiday: empty string"""
        return "" if self._holiday else self._end

    @property
    def duration(self) -> str:
        """Duration of one working day. If holiday: empty string."""
        dur = self.duration_timedelta
        dur = str(dur)[:-3]
        dur = '0' + dur if len(dur) == 4 else dur
        return "" if self._holiday else dur

    @property
    def duration_timedelta(self) -> datetime.timedelta:
        return work_duration_day(self._start, self._end, self._lunch)

    def __str__(self) -> str:
        return f"| {self.date} | {self.weekday} | {self.start} | {self.end} | {self.duration} |"

    def __repr__(self) -> str:
        if not self._holiday:
            repr_string = f"DayRow({self._date.__repr__()}, '{self._start}', '{self._end}', '{self._lunch}')"
        else:
            repr_string = f"DayRow({self._date.__repr__()}, '{self._start}', '{self._end}', '{self._lunch}', "\
                          f"holiday={self._holiday})"
        return repr_string

    @property
    def row(self):
        return [self.date, self.weekday, self.start, self.end, self.duration]


class Table():
    """Write table containing all data in .md file"""

    def __init__(self, obj: Answers) -> None:
        self._name: str = obj.name
        self._year_month: str = obj.year_month
        self._start: str = obj.start_time
        self._end: str = obj.end_time
        self._lunch: str = obj.lunch

    @property
    def current_month_rows(self) -> list[DayRow]:
        year_, month_ = (self._year_month).split("-")
        return generate_day_rows(year_, month_, self._start, self._end, self._lunch)

    @property
    def header(self) -> list:
        lines_header = [
            f"{self._year_month} {self._name}",
            "",
            "<br>",
            "",
            "| **Date** |  | **Start** | **End** | **Duration** |",
            "|:--|--:|--:|--:|--:|--:|",
        ]
        return lines_header

    @property
    def footer(self) -> list:
        lines_footer = [
            "",
            "<br>",
            "",
            f"Lunch: {self._lunch} minutes per one working day",
            "",
            "<br>",
            "",
            "**Total:**",
            "<br>",
            f"**{str(self.total_hours)}**",
        ]
        return lines_footer

    @property
    def month_table(self):
        table = [str(dayrow) for dayrow in self.current_month_rows]
        return table

    @property
    def total_hours(self):
        total_seconds: float = work_duration_total(self.current_month_rows)
        hours: float = (total_seconds//60)//60
        h_to_s = hours * 60 * 60  # hours, converted into seconds
        minutes: float = (total_seconds - h_to_s)/60
        return f"{hours:.0f} hours {minutes:.0f} minutes"

    def write(self, filename: str = None):
        rows = self.header + self.month_table + self.footer
        filename = filename or "working_time_table.md"
        with open(filename, "w", encoding="UTF-8") as file:
            for row in rows:
                file.write(row + "\n")


def main() -> None:
    answers = Answers()

    q_count = 0
    while q_count < 5:
        try:
            if q_count == 0:
                answers.name = input(Question.NAME).strip()
                q_count += 1

            if q_count == 1:
                answers.year_month = input(Question.YEAR_MONTH).strip()
                q_count += 1

            if q_count == 2:
                answers.start_time = input(Question.WORKDAY_START).strip()
                q_count += 1

            if q_count == 3:
                answers.end_time = input(Question.WORKDAY_END).strip()
                q_count += 1

            if q_count == 4:
                answers.lunch = input(Question.LUNCH).strip()
                q_count += 1

                try:
                    work_duration_day(answers.start_time, answers.end_time, answers.lunch)
                except ValueError:
                    print("!!! Time of the start of working day + lunch time cannot be more than"\
                          " or equal to the end time of wirking day. Please enter the correct value")
                    q_count = 2
        except ValueError:
            print("!!! Please enter correct value!")

    table = Table(answers)
    table.write()


if __name__ == "__main__":
    print("* * * WORKING TIME TABLE * * *")

    main()

    print("\n* * * File saved! * * *")
