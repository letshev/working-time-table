# Working Time Table Generator

#### Video Demo: <https://youtu.be/qEpen--pUGQ>

#### Description:

**What is it?**
<br>
This program generates working time table based on a few user-specified parameters in a Markdown file format. These parameters are passed as input from the user through the `input()` function, which is called in `main()`.

**What is it for?**
<br>
In Finnish law there is a requirement - to keep records of working hours. This program can generate a Markdown spreadsheet containing work hours for one calendar month. The Markdown file is easy to convert (not with this program) to other file formats such as pdf or html.

**How it works?** 
<br>
The program consists of several classes and functions: 

<br>
Classes:
<br>

> class `Question`
<br>
Stores questions for the user, str variables.

> class `Answers`
<br>
Stores answers received from the user.

> class `DayRow`
<br>
Stores information about one calendar day.

> class `Table`
<br>
Write the generated data to a file.

<br>
Functions:
<br>

> function `work_duration_day()`
<br>
Calculates the number of working hours per day

> function `work_duration_total()`
<br>
Counts the number of working hours in one calendar month

> function `generate_day_rows()`
<br>
Generates a list of `DayRow` objects, i.e. a list of days, for the specified year and month.

> function `main()`
<br>
Asks questions to the user, gives the received values to an instance of the `Answers` class. 

<br>

When `main()` function is called, instance of `Answers` class is created inside of `main()`. The user answers several questions: enters the name, month and year, the typical start and end time of the working day, and the length of the lunch break. (At the moment, there is no choice of holidays: Saturday and Sunday are used by default, but you can make the day a holiday if you use the `holiday=True` argument while creating an instance of `DayRow` class.) Then the data entered by the user is validated and stored in the `Answers` class. 

<br>

Next, an instance of class `Table` is created, an instance of the `Answers` class is passed to it as an argument. The top and bottom parts of the document and the table (spreadsheet) itself are creates in the instance of `Table`. Function `generate_day_rows()` is used to create this spreadsheet. To get the total number of working hours and minutes for one calendar month, used function `work_duration_total()`.

<br>

The `generate_day_rows()` function creates a list of `DayRow` class objects, each instance of which stores information for one row of the table, converted to the appropriate format: day, weekday, start time / end time of the working day, duration of the working day. When calculating the duration of the working day in the `DayRow` class, `work_duration_day()` function is used.

<br>

Then, `.write()` method of instance of `Table` is called. This method writes document to the file.

<br>

**Examples**

How to generate working time table:

```
# create instance of Answers
answers = Answers()

# take the input
answers.name = "John Doe, Jr."
answers.year_month = "2022-08"
answers.start_time = "09:00"
answers.end_time = "17:00"
answers.lunch = "30"

## another example of input:
answers.name = input(Question.NAME).strip()
...

### or one more example:
answers.name = input("What is your name? ").strip()
...

# create instance of class Table
table = Table(answers)

# write file on disk
table.write()
```