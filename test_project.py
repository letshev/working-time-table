import datetime
import os
import pytest
from project import Question, Answers, DayRow, Table
from project import work_duration_day, work_duration_total, generate_day_rows


def test_class_question():
    assert len(Question.NAME) > 0
    assert len(Question.YEAR_MONTH) > 0
    assert len(Question.WORKDAY_START) > 0
    assert len(Question.WORKDAY_END) > 0
    assert len(Question.LUNCH) > 0


answ = Answers()


def test_answers_name():
    with pytest.raises(ValueError):
        answ.name = ""
    with pytest.raises(ValueError):
        answ.name = " "
    with pytest.raises(ValueError):
        answ.name = "55"
    with pytest.raises(ValueError):
        answ.name = "Nick 55"
    with pytest.raises(ValueError):
        answ.name = "Nick ¶¡@Adams"
    with pytest.raises(ValueError):
        answ.name = "-----"
    with pytest.raises(ValueError):
        answ.name = "___"
    with pytest.raises(ValueError):
        answ.name = " , - ."
    with pytest.raises(ValueError):
        answ.name = ",,,"
    with pytest.raises(ValueError):
        answ.name = "..... ...."
    with pytest.raises(ValueError):
        answ.name = "\'. "

    answ.name = "Pekka Uusitalo"
    assert answ.name == "Pekka Uusitalo"

    answ.name = "Nikolai Letshev"
    assert answ.name == "Nikolai Letshev"

    answ.name = "Mike"
    assert answ.name == "Mike"

    answ.name = "Järi Hääpalönen"
    assert answ.name == "Järi Hääpalönen"

    answ.name = "James Anderson Jr."
    assert answ.name == "James Anderson Jr."

    answ.name = "Caesar D'Arco"
    assert answ.name == "Caesar D'Arco"

    answ.name = "Michael Lee-Jackson"
    assert answ.name == "Michael Lee-Jackson"

    answ.name = "Zachary King-Bowers, Jr."
    assert answ.name == "Zachary King-Bowers, Jr."

    answ.name = "Pekka Lee-Uusitalo"
    assert answ.name == "Pekka Lee-Uusitalo"

    answ.name = "Wang O"
    assert answ.name == "Wang O"

    answ.name = "Jöhan Clark"
    assert answ.name == "Jöhan Clark"

    answ.name = "Järi Huan Carlos del Pedro-Rodrigo Maria D'Anna Kahvinen"
    assert answ.name == "Järi Huan Carlos del Pedro-Rodrigo Maria D'Anna Kahvinen"


def test_answers_year_month():
    with pytest.raises(ValueError):
        answ.year_month = ""
    with pytest.raises(ValueError):
        answ.year_month = " "
    with pytest.raises(ValueError):
        answ.year_month = "55"
    with pytest.raises(ValueError):
        answ.year_month = "1990"
    with pytest.raises(ValueError):
        answ.year_month = "-01"
    with pytest.raises(ValueError):
        answ.year_month = "2022-13"
    with pytest.raises(ValueError):
        answ.year_month = "1999-01"
    with pytest.raises(ValueError):
        answ.year_month = "2051-01"
    with pytest.raises(ValueError):
        answ.year_month = "20500-01"
    with pytest.raises(ValueError):
        answ.year_month = "___"
    with pytest.raises(ValueError):
        answ.year_month = "aaaa-bb"
    with pytest.raises(ValueError):
        answ.year_month = "abcd-01"
    with pytest.raises(ValueError):
        answ.year_month = "2022-bb"
    with pytest.raises(ValueError):
        answ.year_month = "2022--1"

    answ.year_month = "2022-08"
    assert answ.year_month == "2022-08"

    answ.year_month = "2022-12"
    assert answ.year_month == "2022-12"

    answ.year_month = "2023-01"
    assert answ.year_month == "2023-01"


def test_answers_start_time():
    # incorrect numbers in hours and/or minutes
    with pytest.raises(ValueError):
        answ.start_time = "00:60"
    with pytest.raises(ValueError):
        answ.start_time = "24:00"
    with pytest.raises(ValueError):
        answ.start_time = "30:00"
    with pytest.raises(ValueError):
        answ.start_time = "30:30"
    with pytest.raises(ValueError):
        answ.start_time = "32:64"
    with pytest.raises(ValueError):
        answ.start_time = "99:90"
    with pytest.raises(ValueError):
        answ.start_time = "050:900"
    with pytest.raises(ValueError):
        answ.start_time = "010:030"
    # format check
    with pytest.raises(ValueError):
        answ.start_time = ":"
    with pytest.raises(ValueError):
        answ.start_time = " : "
    with pytest.raises(ValueError):
        answ.start_time = "5:20"
    with pytest.raises(ValueError):
        answ.start_time = "5:5"
    with pytest.raises(ValueError):
        answ.start_time = "0:0"
    with pytest.raises(ValueError):
        answ.start_time = "10-30"
    with pytest.raises(ValueError):
        answ.start_time = "10 30"
    with pytest.raises(ValueError):
        answ.start_time = "10.30"
    # non-digit characters
    with pytest.raises(ValueError):
        answ.start_time = "aa:bb"
    with pytest.raises(ValueError):
        answ.start_time = "20?:53"
    with pytest.raises(ValueError):
        answ.start_time = "ten thirty"
    with pytest.raises(ValueError):
        answ.start_time = "nine"
    # correct values
    answ.start_time = "00:00"
    assert answ.start_time == "00:00"

    answ.start_time = "00:01"
    assert answ.start_time == "00:01"

    answ.start_time = "09:00"
    assert answ.start_time == "09:00"

    answ.start_time = "10:00"
    assert answ.start_time == "10:00"

    answ.start_time = "13:59"
    assert answ.start_time == "13:59"

    answ.start_time = "23:59"
    assert answ.start_time == "23:59"


def test_answers_end_time():
    # incorrect numbers in hours and/or minutes
    with pytest.raises(ValueError):
        answ.end_time = "00:60"
    with pytest.raises(ValueError):
        answ.end_time = "24:00"
    with pytest.raises(ValueError):
        answ.end_time = "30:00"
    with pytest.raises(ValueError):
        answ.end_time = "30:30"
    with pytest.raises(ValueError):
        answ.end_time = "32:64"
    with pytest.raises(ValueError):
        answ.end_time = "99:90"
    with pytest.raises(ValueError):
        answ.end_time = "050:900"
    with pytest.raises(ValueError):
        answ.end_time = "010:030"
    # format check
    with pytest.raises(ValueError):
        answ.end_time = ":"
    with pytest.raises(ValueError):
        answ.end_time = " : "
    with pytest.raises(ValueError):
        answ.end_time = "5:20"
    with pytest.raises(ValueError):
        answ.end_time = "5:5"
    with pytest.raises(ValueError):
        answ.end_time = "0:0"
    with pytest.raises(ValueError):
        answ.end_time = "10-30"
    with pytest.raises(ValueError):
        answ.end_time = "10 30"
    with pytest.raises(ValueError):
        answ.end_time = "10.30"
    # non-digit characters
    with pytest.raises(ValueError):
        answ.end_time = "aa:bb"
    with pytest.raises(ValueError):
        answ.end_time = "20?:53"
    with pytest.raises(ValueError):
        answ.end_time = "ten thirty"
    with pytest.raises(ValueError):
        answ.end_time = "nine"
    # correct values
    answ.end_time = "00:00"
    assert answ.end_time == "00:00"

    answ.end_time = "00:01"
    assert answ.end_time == "00:01"

    answ.end_time = "09:00"
    assert answ.end_time == "09:00"

    answ.end_time = "10:00"
    assert answ.end_time == "10:00"

    answ.end_time = "13:59"
    assert answ.end_time == "13:59"

    answ.end_time = "23:59"
    assert answ.end_time == "23:59"


def test_answers_lunch():
    with pytest.raises(ValueError):
        answ.lunch = ""
    with pytest.raises(ValueError):
        answ.lunch = "thirty minutes"
    with pytest.raises(ValueError):
        answ.lunch = "thirty"
    with pytest.raises(ValueError):
        answ.lunch = "   "
    with pytest.raises(ValueError):
        answ.lunch = "..."
    with pytest.raises(ValueError):
        answ.lunch = "/help"
    with pytest.raises(ValueError):
        answ.lunch = "AA"
    with pytest.raises(ValueError):
        answ.lunch = "30 minutes"
    with pytest.raises(ValueError):
        answ.lunch = "30.0"
    with pytest.raises(ValueError):
        answ.lunch = "???"
    with pytest.raises(ValueError):
        answ.lunch = "-30"
    answ.lunch = "30"
    assert answ.lunch == "30"
    answ.lunch = "300"
    assert answ.lunch == "300"
    answ.lunch = "60"
    assert answ.lunch == "60"
    answ.lunch = "15"
    assert answ.lunch == "15"
    answ.lunch = "0"
    assert answ.lunch == "0"


def test_answers__check_empty():
    with pytest.raises(ValueError):
        answ.check_empty("")
    with pytest.raises(ValueError):
        answ.check_empty(" ")
    with pytest.raises(ValueError):
        answ.check_empty("           ")


def test_answers__check_correct_time():
    with pytest.raises(ValueError):
        answ.check_correct_time("")
    with pytest.raises(ValueError):
        answ.check_correct_time(":")
    with pytest.raises(ValueError):
        answ.check_correct_time("0:0")
    with pytest.raises(ValueError):
        answ.check_correct_time("1:1")
    with pytest.raises(ValueError):
        answ.check_correct_time("90:210")
    with pytest.raises(ValueError):
        answ.check_correct_time("24:60")
    with pytest.raises(ValueError):
        answ.check_correct_time("-1:-3")


def test_answers_read_write_properties():
    answ.name = "Nikolas Lee-Cage D'Ateria"
    answ.year_month = "2022-08"
    answ.start_time = "09:00"
    answ.end_time = "17:00"
    answ.lunch = "30"

    assert answ.name == "Nikolas Lee-Cage D'Ateria"
    assert answ.year_month == "2022-08"
    assert answ.start_time == "09:00"
    assert answ.end_time == "17:00"
    assert answ.lunch == "30"


def test_work_duration_day():
    delta: datetime.timedelta = work_duration_day("09:00", "17:00", 30)
    assert delta == datetime.timedelta(hours=7, minutes=30)

    delta: datetime.timedelta = work_duration_day("09:00", "10:00", 30)
    assert delta == datetime.timedelta(hours=0, minutes=30)

    delta: datetime.timedelta = work_duration_day("00:00", "23:30", 30)
    assert delta == datetime.timedelta(hours=23, minutes=0)

    delta: datetime.timedelta = work_duration_day("09:00", "16:43", 30)
    assert delta == datetime.timedelta(hours=7, minutes=13)

    delta: datetime.timedelta = work_duration_day("09:00", "16:43", 30)
    assert delta == datetime.timedelta(hours=7, minutes=13)

    # test incorrect time range
    with pytest.raises(ValueError):
        delta = work_duration_day("16:43", "09:00", 30)
    with pytest.raises(ValueError):
        delta = work_duration_day("16:43", "16:59", 30)
    with pytest.raises(ValueError):
        delta = work_duration_day("16:00", "16:30", 30)
    with pytest.raises(ValueError):
        delta = work_duration_day("23:59", "00:01", 30)


def test_work_duration_total():
    # august 2022  :: 23 workdays
    dayrowlist = generate_day_rows("2022", "08", "09:00", "10:00", "30")
    # one day: 30 min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=11, minutes=30)).total_seconds()
    assert total_sec == timedelta_float

    dayrowlist = generate_day_rows("2022", "08", "09:00", "17:00", "30")
    # one day: 7h 30 min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=172.5)).total_seconds()
    assert total_sec == timedelta_float

    dayrowlist = generate_day_rows("2022", "08", "00:00", "23:30", "30")
    # one day: 23h 0min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=529)).total_seconds()
    assert total_sec == timedelta_float


    # june 2022  :: 22 workdays
    dayrowlist = generate_day_rows("2022", "06", "09:00", "10:00", "30")
    # one day: 30 min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=11)).total_seconds()
    assert total_sec == timedelta_float

    dayrowlist = generate_day_rows("2022", "06", "09:00", "17:00", "30")
    # one day: 7h 30 min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=165)).total_seconds()
    assert total_sec == timedelta_float

    dayrowlist = generate_day_rows("2022", "06", "00:00", "23:30", "30")
    # one day: 23h 0min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=506)).total_seconds()
    assert total_sec == timedelta_float


    # february 2022  :: 20 workdays
    dayrowlist = generate_day_rows("2022", "02", "09:00", "10:00", "30")
    # one day: 30 min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=10)).total_seconds()
    assert total_sec == timedelta_float

    dayrowlist = generate_day_rows("2022", "02", "09:00", "17:00", "30")
    # one day: 7h 30 min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=150)).total_seconds()
    assert total_sec == timedelta_float

    dayrowlist = generate_day_rows("2022", "02", "00:00", "23:30", "30")
    # one day: 23h 0min
    total_sec = work_duration_total(dayrowlist)
    timedelta_float = (datetime.timedelta(hours=460)).total_seconds()
    assert total_sec == timedelta_float


def test_generate_day_rows():
    # august 2022
    dayrowlist = generate_day_rows("2022", "08", "09:00", "10:00", "30")
    assert len(dayrowlist) > 0
    assert len(dayrowlist) == 31
    holidays = [day for day in dayrowlist if day.holiday]
    assert len(holidays) == 8
    workdays = [day for day in dayrowlist if not day.holiday]
    assert len(workdays) == 23
    # check if start and end time for holidays is empty value
    holidays_stime_etime = [(day.start, day.end) for day in holidays if day.start and day.end]
    assert holidays_stime_etime == []
    # check if start and end time for workdays is not empty
    workday_stime_etime = [(day.start, day.end) for day in workdays if day.start and day.end]
    assert len(workday_stime_etime) > 0
    assert len(workday_stime_etime) == 23

    # june 2022
    dayrowlist = generate_day_rows("2022", "06", "09:00", "10:00", "30")
    assert len(dayrowlist) > 0
    assert len(dayrowlist) == 30
    holidays = [day for day in dayrowlist if day.holiday]
    assert len(holidays) == 8
    workdays = [day for day in dayrowlist if not day.holiday]
    assert len(workdays) == 22
    # check if start and end time for holidays is empty value
    holidays_stime_etime = [(day.start, day.end) for day in holidays if day.start and day.end]
    assert holidays_stime_etime == []
    # check if start and end time for workdays is not empty
    workday_stime_etime = [(day.start, day.end) for day in workdays if day.start and day.end]
    assert len(workday_stime_etime) > 0
    assert len(workday_stime_etime) == 22

    # february 2022
    dayrowlist = generate_day_rows("2022", "02", "09:00", "10:00", "30")
    assert len(dayrowlist) > 0
    assert len(dayrowlist) == 28
    holidays = [day for day in dayrowlist if day.holiday]
    assert len(holidays) == 8
    workdays = [day for day in dayrowlist if not day.holiday]
    assert len(workdays) == 20
    # check if start and end time for holidays is empty value
    holidays_stime_etime = [(day.start, day.end) for day in holidays if day.start and day.end]
    assert holidays_stime_etime == []
    # check if start and end time for workdays is not empty
    workday_stime_etime = [(day.start, day.end) for day in workdays if day.start and day.end]
    assert len(workday_stime_etime) > 0
    assert len(workday_stime_etime) == 20


def test_class_dayrow():
    day = DayRow(date=datetime.date(2022, 2, 1), start="09:00", end="10:00", lunch="30")
    assert day.row == ["01-Feb-2022", "Tuesday", "09:00", "10:00", "00:30"]
    assert str(day) == "| 01-Feb-2022 | Tuesday | 09:00 | 10:00 | 00:30 |"

    day = DayRow(date=datetime.date(2020, 2, 29), start="09:00", end="10:00", lunch="30")
    assert day.row == ["29-Feb-2020", "Saturday", "", "", ""]
    assert str(day) == "| 29-Feb-2020 | Saturday |  |  |  |"

    with pytest.raises(AttributeError):
        day.date = 1
    with pytest.raises(AttributeError):
        day.start = 1
    with pytest.raises(AttributeError):
        day.end = 1
    with pytest.raises(AttributeError):
        day.weekday = 1
    with pytest.raises(AttributeError):
        day.holiday = 1
    with pytest.raises(AttributeError):
        day.duration = 1
    with pytest.raises(AttributeError):
        day.duration_timedelta = 1
    with pytest.raises(AttributeError):
        day.row = 1


def test_class_table():
    answ = Answers()
    answ.name = "Nikolas Lee-Cage D'Ateria"
    answ.year_month = "2022-08"
    answ.start_time = "09:00"
    answ.end_time = "17:00"
    answ.lunch = "30"

    # instance of class `Table` could be created
    table = Table(answ)
    assert table._name == "Nikolas Lee-Cage D'Ateria"
    assert table._year_month == "2022-08"
    assert table._start == "09:00"
    assert table._end == "17:00"
    assert table._lunch == "30"
    assert table.header[0] == "2022-08 Nikolas Lee-Cage D'Ateria"
    assert table.footer[3] == "Lunch: 30 minutes per one working day"
    assert table.footer[-1] == "**172 hours 30 minutes**"

    ## check that month_table works correct
    assert table.month_table[0] == "| 01-Aug-2022 | Monday | 09:00 | 17:00 | 07:30 |"
    assert table.month_table[14] == "| 15-Aug-2022 | Monday | 09:00 | 17:00 | 07:30 |"
    assert table.month_table[29] == "| 30-Aug-2022 | Tuesday | 09:00 | 17:00 | 07:30 |"
    assert table.month_table[30] == "| 31-Aug-2022 | Wednesday | 09:00 | 17:00 | 07:30 |"

    ## check that total_hours works correct
    assert table.total_hours == "172 hours 30 minutes"

    # write method works correcly
    ## delete old files before the test
    for file in ("working_time_table.md", "this_is_cs50.md"):
        if os.path.isfile(file):
            os.remove(file)

    assert os.path.exists("working_time_table.md") is False
    table.write()
    assert os.path.exists("working_time_table.md") is True
    os.remove("working_time_table.md")

    assert os.path.exists("this_is_cs50.md") is False
    table.write(filename="this_is_cs50.md")
    assert os.path.exists("this_is_cs50.md") is True
    os.remove("this_is_cs50.md")

    with pytest.raises(AttributeError):
        table.header = 1
    with pytest.raises(AttributeError):
        table.footer = 1
    with pytest.raises(AttributeError):
        table.month_table = 1
    with pytest.raises(AttributeError):
        table.total_hours = 1
    with pytest.raises(AttributeError):
        table.current_month_rows = 1
